using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameController : MonoBehaviour {


	public static GameController Instance;
	public bool isPlayerTurn = false;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	public static bool isPlayer1;
	public static bool isPlayer2;

	private GameObject startImage;
	private GameObject button;
	private Text startText;

	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 1;
	private GameObject player1Button;
	private GameObject player2Button;
	private GameObject startScreen;
	
	void Awake () {

		if (Instance != null && Instance != this) 
		{
			DestroyImmediate (gameObject);
			return ;
		}

		Instance = this;
		DontDestroyOnLoad (gameObject);
		boardController = GetComponent<BoardController> ();
		enemies = new List<Enemy>();

	
	}

	public void Player1() {
		isPlayer1 = true;
		player1Button.SetActive (false);
		player2Button.SetActive (false);
		startScreen.SetActive (false);
		InitializeGame ();
	}

	public void Player2() {
		isPlayer2 = true;
		Player.animator.SetTrigger ("player2");
		player1Button.SetActive (false);
		player2Button.SetActive (false);
		startScreen.SetActive (false);
		InitializeGame ();
	}

	void Start()
	{
		levelImage = GameObject.Find ("Level Image");
		player1Button = GameObject.Find ("PLayer 1");
		player2Button = GameObject.Find ("Player 2");
		startScreen = GameObject.Find ("Start Image");
		//InitializeGame ();
	}

	private void InitializeGame()
	{
		isPlayerTurn = true;
		settingUpGame = true;
		levelImage = GameObject.Find ("Level Image");
		levelText = GameObject.Find ("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel ;
		levelImage.SetActive (true);
		enemies.Clear();
		boardController.SetupLevel (currentLevel);
		if (currentLevel != 1) {
			startScreen = GameObject.Find ("Start Image");
			startScreen.SetActive (false);
		}
	    Invoke ("DisableLevelImage", secondsUntilLevelStart);
	}
	

	private void DisableLevelImage()
	{
		if (isPlayer2) {
			Player.animator.SetTrigger ("player2");
		}
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
		levelImage.SetActive (false);
		if (currentLevel != 1) {

		}
	}

	private void OnLevelWasLoaded(int levelLoaded)
	{
		currentLevel++;
		InitializeGame ();
	}

	void Update () {
		if (isPlayerTurn || areEnemiesMoving || settingUpGame)
		{
			return;
		}

		StartCoroutine (MovingEnemies ());
	}

	private IEnumerator MovingEnemies()
	{
		areEnemiesMoving = true;

		yield return new WaitForSeconds (0.2f);

		foreach (Enemy enemy in enemies) 
		{
			enemy.MoveEnemy ();
			yield return new WaitForSeconds (enemy.moveTime);
		}

		areEnemiesMoving = false;
		isPlayerTurn = true;
	}

	public void AddEnemyToList(Enemy enemy)
	{
		enemies.Add (enemy);
	}
	

	public void GameOver()
	{
		isPlayerTurn = false;
		SoundController.Instance.music.Stop ();
		SoundController.Instance.PlaySingle(gameOverSound);
		levelImage.SetActive (true);
		levelText.text = "You starved after " + currentLevel + " days....";

		enabled = false;
		/*if(playerHealth<=0){enabled = true;}*/
	}
}

